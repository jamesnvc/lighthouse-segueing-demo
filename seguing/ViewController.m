//
//  ViewController.m
//  seguing
//
//  Created by James Cash on 12-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"Preparing %@", segue);
    if ([[segue identifier] isEqualToString:@"showStuff"]) {
        UIViewController *dest = [segue destinationViewController];
        UILabel *label = (UILabel *)[dest.view viewWithTag:42];
        label.text = self.typedStuffField.text;
    }
}

@end
