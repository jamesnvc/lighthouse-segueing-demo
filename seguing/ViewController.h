//
//  ViewController.h
//  seguing
//
//  Created by James Cash on 12-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *typedStuffField;


@end

